from django.apps import AppConfig


class SionControllerConfig(AppConfig):
    name = 'Sion_Controller'
