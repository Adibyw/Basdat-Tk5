from django.shortcuts import render, redirect, reverse
from django.contrib import messages
from django.views.decorators.csrf import csrf_exempt
from django.db import connection

def profile_user(request):
    if 'logged_in' not in request.session or not request.session['logged_in']:
        return render(request, 'login/login.html')
    else:
        cursor = connection.cursor()
        if(request.session['role'] == 'donatur'):
            profile_donator(request)

        elif(request.session['role'] == 'sponsor'):
            profile_sponsor(request)

        elif(request.session.role == 'relawan'):
            profile_relawan(request)

        elif(request.session.role == 'pengurus'):
            profile_pengurus(request)

def profile_donator(request):
    email = request.session.get('email')

    cursor.execute("SELECT P.nama FROM SION.PENGGUNA P WHERE P.email = '"+email+"'")
    select = cursor.fetchone()
    response['nama'] = select[0]

    cursor.execute("SELECT D.email FROM SION.DONATUR D WHERE D.email = '"+email+"'")
    select = cursor.fetchone()
    response['email'] = select[0]

    cursor.execute("SELECT P.alamat_lengkap FROM SION.DONATUR D, SION.PENGGUNA P WHERE P.email = D.email AND D.email = '"+email+"'")
    select = cursor.fetchone()
    response['alamat'] = select[0]

    cursor.execute("SELECT D.saldo FROM SION.DONATUR D WHERE D.email = '"+email+"'")
    select = cursor.fetchone()
    response['saldo'] = select[0]

    return render(request, 'profile/profile_donator.html', response)

def profile_sponsor(request):
    email = request.session.get('email')

    cursor.execute("SELECT P.nama FROM SION.SPONSOR S, SION.PENGGUNA P WHERE S.email = P.email AND S.email = '"+email+"'")
    select = cursor.fetchone()
    response['nama'] = select[0]

    cursor.execute("SELECT S.email FROM SION.SPONSOR S WHERE S.email = '"+email+"'")
    select = cursor.fetchone()
    response['email'] = select[0]

    cursor.execute("SELECT P.alamat_lengkap FROM SION.SPONSOR S, SION.PENGGUNA P WHERE S.email = P.email AND S.email = '"+email+"'")
    select = cursor.fetchone()
    response['alamat'] = select[0]

    return render(request, 'profile/profile_sponsor.html', response)

def profile_relawan(request):
    email = request.session.get('email')

    cursor.execute("SELECT P.nama FROM SION.RELAWAN R, SION.PENGGUNA P WHERE P.email = R.email AND R.email = '"+email+"'")
    select = cursor.fetchone()
    response['nama'] = select[0]

    cursor.execute("SELECT R.email FROM SION.RELAWAN R WHERE R.email = '"+email+"'")
    select = cursor.fetchone()
    response['email'] = select[0]

    cursor.execute("SELECT R.no_hp FROM SION.RELAWAN R WHERE R.email = '"+email+"'")
    select = cursor.fetchone()
    response['no_hp'] = select[0]

    cursor.execute("SELECT R.tanggal_lahir FROM SION.RELAWAN R WHERE R.email = '"+email+"'")
    select = cursor.fetchone()
    response['tgl_lahir'] = select[0]

    cursor.execute("SELECT P.alamat_lengkap FROM SION.RELAWAN R, SION.PENGGUNA P WHERE R.email = P.email AND R.email = '"+email+"'")
    select = cursor.fetchone()
    response['alamat'] = select[0]

    get_keahlian_relawan()
    
    return render(request, 'profile/profile_relawan.html', response)

def get_keahlian_relawan():
    cursor.execute("SELECT KR.keahlian FROM SION.KEAHLIAN_RELAWAN KR WHERE KR.email = '"+email+"'")
    select = cursor.fetchone()

    lst = []
    for i in range(len(select)):
    	lst[i] = select[i]

    response['keahlian'] = lst

def profile_pengurus(request):
    email = request.session.get('email')

    cursor.execute("SELECT P.nama FROM SION.PENGURUS_ORGANISASI O, SION.PENGGUNA P WHERE O.email = R.email AND O.email = '"+email+"'")
    select = cursor.fetchone()
    response['nama'] = select[0]

    cursor.execute("SELECT O.email FROM SION.PENGURUS_ORGANISASI O WHERE O.email = '"+email+"'")
    select = cursor.fetchone()
    response['email'] = select[0]

    cursor.execute("SELECT P.alamat_lengkap FROM SION.PENGURUS_ORGANISASI O, SION.PENGGUNA P WHERE O.email = P.email AND O.email = '"+email+"'")
    select = cursor.fetchone()
    response['alamat'] = select[0]
    return render(request, 'profile/profile_pengurus.html', response)