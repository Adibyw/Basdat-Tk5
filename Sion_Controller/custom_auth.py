from django.shortcuts import render, redirect, reverse
from django.contrib import messages
from django.views.decorators.csrf import csrf_exempt
from django.db import connection



response = {}
#authentication
@csrf_exempt
def auth_login(request):
    print ("#==> auth_login ")

    if 'logged_in' not in request.session or not request.session['logged_in']:
        if request.method == 'POST':
            cursor=connection.cursor()
            email = request.POST['email']
            password = request.POST['password']
            cursor.execute("SELECT * from SION.PENGGUNA where EMAIL='"+email+"'")
            select=cursor.fetchone()
            if select:
                cursor.execute("SELECT password from SION.PENGGUNA where EMAIL='"+email+"'")
                select=cursor.fetchone()
                if select[0] == password:
                    request.session['logged_in'] = True
                    request.session.modified = True
                    request.session["email"]=email

                    cursor.execute("SELECT nama from SION.PENGGUNA where EMAIL='"+email+"'")
                    select=cursor.fetchone()
                    request.session["nama"]=select[0]

                    cursor.execute("SELECT * from SION.RELAWAN where EMAIL='"+email+"'")
                    select=cursor.fetchone()
                    if (select):
                        request.session["role"]="relawan"
                        messages.success(request, "Anda telah berhasil login kedalam sistem")
                        return redirect(reverse('Sion-Controller:index'))

                    cursor.execute("SELECT * from SION.DONATUR where EMAIL='"+email+"'")
                    select=cursor.fetchone()
                    if (select):
                        request.session["role"]="donatur"
                        messages.success(request, "Anda telah berhasil login kedalam sistem")
                        return redirect(reverse('Sion-Controller:index'))

                    cursor.execute("SELECT * from SION.SPONSOR where EMAIL='"+email+"'")
                    select=cursor.fetchone()
                    if (select):
                        request.session["role"]="sponsor"
                        messages.success(request, "Anda telah berhasil login kedalam sistem")
                        return redirect(reverse('Sion-Controller:index'))

                    #IMPLEMENT PENGURUS HERE AND ORGANISASI
                    cursor.execute("SELECT * from SION.PENGURUS_ORGANISASI where EMAIL='"+email+"'")
                    select=cursor.fetchone()
                    if (select):
                        request.session["role"]="pengurus"
                        messages.success(request, "Anda telah berhasil login kedalam sistem")
                        return redirect(reverse('Sion-Controller:index'))

                else:
                    messages.error(request, 'Incorrect email or password.')
                    response['email'] = request.POST['email']
                    return render(request, 'login/login.html', response)
            else:
                messages.error(request, 'Incorrect email or password.')
                response['email'] = request.POST['email']
                return render(request, 'login/login.html', response)
        else:
            return render(request,'login/login.html')

    else:
        messages.info(request,'Anda sudah masuk kedalam sistem.')
        return redirect(reverse('Sion-Controller:index'))


def auth_logout(request):
    print ("#==> auth logout")
    request.session.flush() # menghapus semua session

    messages.info(request, "Anda berhasil logout. Semua session Anda sudah dihapus")
    return HttpResponseRedirect(reverse('Sion-Controller:index'))