from django.conf.urls import url
from .views import index, login, register_relawan, register_donatur, register_sponsor
from .custom_auth import auth_login, auth_logout
from .register_organisasi import register_organisasi, reg_organisasi
from .profile import profile_user

urlpatterns = [
    #Landing Page
    url(r'^$', index, name='index'),
    url(r'^login/$', login, name='login'),

    # custom auth
    url(r'^custom_auth/login/$', auth_login, name='auth_login'),
    url(r'^custom_auth/logout/$', auth_logout, name='auth_logout'),

    #Register Organisasi
    url(r'^register_organisasi/$', register_organisasi, name='register-organisasi'),
	url(r'^reg_organisasi/$', reg_organisasi, name='reg-organisasi'),

	#Register Pengguna
	url(r'^register_Relawan/$', register_relawan, name='register-relawan'),
	url(r'^register_Donatur/$', register_donatur, name='register-donatur'),
	url(r'^register_Sponsor/$', register_sponsor, name='register-sponsor'),

	#Profile
	url(r'^profile/$', profile_user, name='profile'),
]