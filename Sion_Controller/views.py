from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse

# Create your views here.
response = {}

def index(request):
    print ("#==> masuk index")
    if 'user_login' in request.session:
        return HttpResponseRedirect(reverse('Sion-Controller:profile'))
    else:
        response['login'] = False
        html = 'login/landing.html'
        return render(request, html, response)


def login(request):
    html = 'login/login.html'
    return render(request, html, response)

def register_relawan(request):
    html = 'register/register_relawan.html'
    return render(request, html)

def register_donatur(request):
    html = 'register/register_donatur.html'
    return render(request, html)

def register_sponsor(request):
    html = 'register/register_sponsor.html'
    return render(request, html)